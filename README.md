# classProject
Branden Bohling

My app will be a game used to study Geography and the flags of the world

Features:
1. Swipeable display with pictures, each swipe displays a new flag
2. Randomized flag displayed, every time the app starts up, the flags will be in different order
3. Tapping on the picture will reveal which country that flag belongs to
4. Country will be displayed alongside a map of whatever country that is
5. You can flip the flashcard back and forth, infinate times from flag side view to answer side view